import 'package:qwil_flutter_test/domain/entity/entity.dart';
import 'package:qwil_flutter_test/domain/entity/json_serializable.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';

class ChatEntity implements JsonSerializable<ChatEntity>, Entity {
  final String id;
  final List<String> userIds;
  final List<MessageEntity> messages;

  ChatEntity(this.id, this.messages, this.userIds);

  @override
  String getId() {
    return id;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ChatEntity &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          userIds == other.userIds &&
          messages == other.messages;

  @override
  int get hashCode => id.hashCode ^ userIds.hashCode ^ messages.hashCode;

  Map<String, Object> toJson() {
    return {"messages": messages, "id": id, "userIDs": userIds};
  }

  @override
  String toString() {
    return 'DialogEntity{messages: $messages, id: $id}';
  }

  @override
  ChatEntity fromJson(Map<String, Object> json) {
    return ChatEntity(
        json["id"] as String,
        json["messages"] as List<MessageEntity>,
        json["userIDs"] as List<String>);
  }
}
