import 'package:qwil_flutter_test/domain/entity/entity.dart';
import 'package:qwil_flutter_test/domain/entity/json_serializable.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';

class SummaryEntity extends Entity implements JsonSerializable<SummaryEntity> {
  final String id;
  final List<MessageEntity> messages;

  SummaryEntity(this.id, this.messages);

  factory SummaryEntity.empty() =>
      SummaryEntity("d", [
        MessageEntity("0", "0", "n/a"),
        MessageEntity("0", "0", "n/a"),
        MessageEntity("0", "0", "n/a")
      ]);

  @override
  String getId() {
    return id;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SummaryEntity &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          messages == other.messages;

  @override
  int get hashCode => id.hashCode ^ messages.hashCode;

  @override
  Map<String, Object> toJson() {
    return {"messages": messages, "id": id};
  }

  @override
  SummaryEntity fromJson(Map<String, Object> json) {
    return SummaryEntity(
      json["id"] as String,
      json["messages"] as List<MessageEntity>,
    );
  }
}
