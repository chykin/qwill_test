import 'package:qwil_flutter_test/domain/entity/entity.dart';
import 'package:qwil_flutter_test/domain/entity/json_serializable.dart';

class MessageEntity extends Entity implements JsonSerializable<MessageEntity> {
  final String id;
  final String userId;
  final String text;

  MessageEntity(this.id, this.userId, this.text);

  @override
  String getId() {
    return id;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is MessageEntity &&
              runtimeType == other.runtimeType &&
              id == other.id &&
              userId == other.userId &&
              text == other.text;

  @override
  int get hashCode =>
      id.hashCode ^
      userId.hashCode ^
      text.hashCode;

  @override
  Map<String, Object> toJson() {
    return {
      "text": text,
      "id": id,
      "user_id":userId
    };
  }

  @override
  String toString() {
    return 'MessageEntity{user: $userId, id: $id, text: $text}';
  }

  @override
   MessageEntity fromJson(Map<String, Object> json) {
    return MessageEntity(
      json["id"] as String,
      json["user_id"] as String,
      json["text"] as String
    );
  }

}
