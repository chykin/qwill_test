import 'package:flutter/widgets.dart';

class ArchSampleKeys {
  static final homeScreen = const Key('__homeScreen__');
  static final messageList = const Key('__messageList__');
  static final startStop = const Key('__startStop__');
  static final summary = const Key('__summary__');
  static final loading = const Key('__loading__');
}
