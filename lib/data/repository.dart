

import 'dart:async';
import 'dart:core';

import 'package:qwil_flutter_test/data/file_storage.dart';
import 'package:qwil_flutter_test/data/web_client.dart';
import 'package:qwil_flutter_test/domain/entity/json_serializable.dart';
import 'package:qwil_flutter_test/domain/repo/repository.dart';
/// A class that glues together our local file storage and web client. It has a
/// clear responsibility: Load Todos and Persist todos.
class BaseRepositoryFlutter<T extends JsonSerializable> implements Repository<T> {
  final FileStorage<T> fileStorage;
  final WebClient<T> webClient;

  const BaseRepositoryFlutter(this.fileStorage, this.webClient);

  /// Loads todos first from File storage. If they don't exist or encounter an
  /// error, it attempts to load the Todos from a Web Client.
  @override
  Future<List<T>> loadItems() async {
    try {
      return await fileStorage.loadItems();
    } catch (e) {
      final todos = await webClient.fetch();

      fileStorage.saveItems(todos);

      return todos;
    }
  }

  // Persists todos to local disk and the web
  @override
  Future saveItems(List<T> items) {
    return Future.wait<dynamic>([
      fileStorage.saveItems(items),
      webClient.postTodos(items),
    ]);
  }
}
