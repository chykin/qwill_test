import 'package:qwil_flutter_test/data/api.dart';
import 'package:qwil_flutter_test/data/messages/messages_generator.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';

/*
* feel generators by user id from chat
* chat.userIds.forEach((userID){
      messagesGenerators[userID] =  MessagesGenerator(userID, Randomizer());
    });
* */
class MessageApi implements Api<MessageEntity> {
  final Map<String, MessagesGenerator> messagesGenerators;

   MessageApi(this.messagesGenerators);

  @override
  List<MessageEntity> getAll() {
    return [
      MessageEntity("messageID0", "userID1", "message from user id 1"),
      MessageEntity("messageID1", "userID2", "message from user id 2"),
      MessageEntity("messageID2", "userID3", "message from user id 3"),
      MessageEntity("messageID3", "userID4", "message from user id 4"),
    ];
  }

  void start() {
    messagesGenerators.values.forEach(doStart);
  }

  void stop() {
    messagesGenerators.values.forEach(doStop);
  }

  Stream<MessageEntity> getByUserID(String userID) {
    return messagesGenerators[userID].messagesStream.stream;
  }

  void doStart(MessagesGenerator element) {
    element.startGenerating();
  }

  void doStop(MessagesGenerator element) {
    element.stopGenerating();
  }
}
