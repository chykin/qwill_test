import 'dart:core';

import 'package:qwil_flutter_test/data/file_storage.dart';
import 'package:qwil_flutter_test/data/messages/messages_web_client.dart';
import 'package:qwil_flutter_test/data/repository.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';

class MessageRepositoryFlutter extends BaseRepositoryFlutter<MessageEntity> {
  final MessageWebClient messageWebClient;

  const MessageRepositoryFlutter(
      FileStorage<MessageEntity> fileStorage, this.messageWebClient)
      : super(fileStorage, messageWebClient);

  Stream<MessageEntity> getByUserID(String userID) {
    return messageWebClient.messageApi.getByUserID(userID);
  }


}
