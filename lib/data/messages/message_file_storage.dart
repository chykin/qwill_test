import 'package:qwil_flutter_test/data/file_storage.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';

/// Loads and saves a List of Todos using a text file stored on the device.
///
/// Note: This class has no direct dependencies on any Flutter dependencies.
/// Instead, the `getDirectory` method should be injected. This allows for
/// testing.
class MessageFileStorage extends FileStorage<MessageEntity>{
  const MessageFileStorage(String tag, getDirectory) : super(tag, getDirectory);
}
