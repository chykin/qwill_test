import 'package:qwil_flutter_test/data/messages/randomizer.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';
import 'package:qwil_flutter_test/domain/uuid.dart';
import 'package:rxdart/rxdart.dart';

class MessagesGenerator {
  final String userID;
  final Randomizer randomizer;

  final PublishSubject<MessageEntity> messagesStream =
      PublishSubject<MessageEntity>();
  final Uuid uuid = Uuid();

  var generatorSubscription;

  MessagesGenerator(this.userID, this.randomizer);

  void startGenerating() {
    generatorSubscription =
        Observable(Stream.periodic(Duration(seconds: 1), (q) => q))
        .concatMap((i){
          return Observable.just(i)
              .delay(Duration(seconds: randomizer.getRandomDelay()));
        })
            .listen(publish);
  }

  void stopGenerating() {
    generatorSubscription.cancel();
  }

  Observable<MessageEntity> getMessage() {
    return messagesStream;
  }

  void publish(dynamic event) {
    messagesStream.add(MessageEntity(
        uuid.generateV4(), userID, "message $event from $userID"));
  }
}
