 

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:qwil_flutter_test/domain/entity/json_serializable.dart';


/// Loads and saves a List of Todos using a text file stored on the device.
///
/// Note: This class has no direct dependencies on any Flutter dependencies.
/// Instead, the `getDirectory` method should be injected. This allows for
/// testing.
class FileStorage<T extends JsonSerializable > {
  final String tag;
  final Future<Directory> Function() getDirectory;

  const FileStorage(
    this.tag,
    this.getDirectory,
  );

  Future<List<T>> loadItems() async {
    final file = await _getLocalFile();
    final string = await file.readAsString();
    final json = JsonDecoder().convert(string);
    final items = (json[tag])
        .map<T>((item) => item.fromJson(item))
        .toList();

    return items;
  }

  Future<File> saveItems(List<T> items) async {
    final file = await _getLocalFile();

    return file.writeAsString(JsonEncoder().convert({
      tag: items.map((item) => item.toJson()).toList(),
    }));
  }

  Future<File> _getLocalFile() async {
    final dir = await getDirectory();

    return File('${dir.path}/ArchSampleStorage__$tag.json');
  }

  Future<FileSystemEntity> clean() async {
    final file = await _getLocalFile();

    return file.delete();
  }
}
