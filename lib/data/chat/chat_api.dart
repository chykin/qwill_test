import 'package:qwil_flutter_test/data/api.dart';
import 'package:qwil_flutter_test/domain/entity/chat_entity.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';

class ChatApi implements Api<ChatEntity> {

  const ChatApi();

  @override
  List<ChatEntity> getAll() {
    return [
      ChatEntity(
          "dialogID1",
          [
            MessageEntity("messageID0", "userID2", "message from user id 2"),
            MessageEntity("messageID1", "userID2", "message from user id 2"),
            MessageEntity("messageID2", "userID3", "message from user id 3"),
            MessageEntity("messageID3", "userID4", "message from user id 4")
          ],
          ["userID0", "userID1", "userID2"])
    ];
  }
}
