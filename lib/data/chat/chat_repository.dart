import 'dart:core';

import 'package:qwil_flutter_test/data/file_storage.dart';
import 'package:qwil_flutter_test/data/repository.dart';
import 'package:qwil_flutter_test/data/web_client.dart';
import 'package:qwil_flutter_test/domain/entity/chat_entity.dart';

class ChatRepositoryFlutter extends BaseRepositoryFlutter<ChatEntity> {
  ChatRepositoryFlutter(
      FileStorage<ChatEntity> fileStorage, WebClient<ChatEntity> webClient)
      : super(fileStorage, webClient);


}
