 

import 'package:qwil_flutter_test/data/api.dart';
import 'package:qwil_flutter_test/data/web_client.dart';
import 'package:qwil_flutter_test/domain/entity/chat_entity.dart';

/// A class that is meant to represent a Client that would be used to call a Web
/// Service. It is responsible for fetching and persisting Todos to and from the
/// cloud.
///
/// Since we're trying to keep this example simple, it doesn't communicate with
/// a real server but simply emulates the functionality.
class ChatWebClient extends WebClient<ChatEntity>{
  ChatWebClient(Api<ChatEntity> api, Duration duration) : super(api, duration);

}
