 

import 'dart:async';

import 'package:qwil_flutter_test/data/api.dart';
/// A class that is meant to represent a Client that would be used to call a Web
/// Service. It is responsible for fetching and persisting Todos to and from the
/// cloud.
///
/// Since we're trying to keep this example simple, it doesn't communicate with
/// a real server but simply emulates the functionality.
class WebClient<T> {
  final Duration delay;
  final Api<T> api;
  WebClient(this.api, [this.delay = const Duration(milliseconds: 3000)]);

  Future<List<T>> fetch() async {
    return Future.delayed(
        delay,
        () => api.getAll());
  }

  /// Mock that returns true or false for success or failure. In this case,
  /// it will "Always Succeed"
  Future<bool> postTodos(List<T> items) async {
    return Future.value(true);
  }
}
