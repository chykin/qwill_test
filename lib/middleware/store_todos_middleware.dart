import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qwil_flutter_test/actions/actions.dart';
import 'package:qwil_flutter_test/data/chat/chat_api.dart';
import 'package:qwil_flutter_test/data/chat/chat_file_storage.dart';
import 'package:qwil_flutter_test/data/chat/chat_repository.dart';
import 'package:qwil_flutter_test/data/chat/chat_web_client.dart';
import 'package:qwil_flutter_test/data/messages/message_api.dart';
import 'package:qwil_flutter_test/data/messages/message_file_storage.dart';
import 'package:qwil_flutter_test/data/messages/message_repository.dart';
import 'package:qwil_flutter_test/data/messages/messages_generator.dart';
import 'package:qwil_flutter_test/data/messages/messages_web_client.dart';
import 'package:qwil_flutter_test/data/messages/randomizer.dart';
import 'package:qwil_flutter_test/domain/entity/chat_entity.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';
import 'package:qwil_flutter_test/domain/entity/summary_entity.dart';
import 'package:qwil_flutter_test/domain/repo/repository.dart';
import 'package:qwil_flutter_test/domain/uuid.dart';
import 'package:qwil_flutter_test/models/models.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

List<Middleware<AppState>> createMiddleWares() {
  var repo = ChatRepositoryFlutter(
      ChatFileStorage(
        '__redux_app__',
        getApplicationDocumentsDirectory,
      ),
      ChatWebClient(const ChatApi(), Duration(seconds: 3)));

  Map<String, MessagesGenerator> messagesGenerators = {};

  messagesGenerators["userID0"] =
      MessagesGenerator("userID0", const Randomizer());
  messagesGenerators["userID1"] =
      MessagesGenerator("userID1", const Randomizer());
  messagesGenerators["userID2"] =
      MessagesGenerator("userID2", const Randomizer());
  messagesGenerators["userID3"] =
      MessagesGenerator("userID3", const Randomizer());

  var messageApi = MessageApi(messagesGenerators);
  MessageRepositoryFlutter messageRepo = MessageRepositoryFlutter(
      MessageFileStorage('__redux_app__', getApplicationDocumentsDirectory),
      MessageWebClient(messageApi, Duration(seconds: 2)));

  return [
    new EpicMiddleware(combineEpics(
      <Epic<AppState>>[
        summaryEpic(messageRepo),
        observeMessagesEpic(messageRepo)
      ],
    )),
    createLoadChatsMiddleware(repo),
    createSaveChatMiddleware(repo),
    createStartMiddleware(messageApi),
    createStopMiddleware(messageApi),
    new TypedMiddleware((Store<AppState> store, action, NextDispatcher next) {
      debugPrint(action.runtimeType.toString());
      next(action);
    })
  ];
}

summaryEpic(MessageRepositoryFlutter messageRepo) {
  return (Stream<dynamic> actions, EpicStore<AppState> store) {
    final action =
        Observable(actions).ofType(TypeToken<ChatsLoadedAction>());
    var userIDS = action.map((action) => (action.chats[0].userIds));
    return userIDS.map((ids) {
      List<Stream<MessageEntity>> streams = [];
      for (var userID in ids) {
        streams.add(messageRepo.getByUserID(userID));
      }
      return streams;
    }).flatMap((streams) {
      return new CombineLatestStream(streams, summaryCombiner);
    }).map((stats) => SummaryReadyAction(stats));
  };
}

observeMessagesEpic(MessageRepositoryFlutter messageRepo) {
  return (Stream<dynamic> actions, EpicStore<AppState> store) {
    final action =
        Observable(actions).ofType(TypeToken<ChatsLoadedAction>());
    var userIDS = action.map((action) => (action.chats[0].userIds));
    return userIDS.map((ids) {
      List<Stream<MessageEntity>> streams = [];
      for (var userID in ids) {
        streams.add(messageRepo.getByUserID(userID));
      }
      return streams;
    }).flatMap((streams) {
      return new MergeStream(streams);
    }).map((message) => NewMessageAction(message));
  };
}

summaryCombiner(MessageEntity a, MessageEntity b,
    [MessageEntity c, d, e, f, g, h, i]) {
  Uuid uuid = Uuid();
  return SummaryEntity(uuid.generateV4(), [a, b, c]);
}

Middleware<AppState> createStartMiddleware(MessageApi messageApi) {
  return TypedMiddleware<AppState, StartAction>(
      (Store<AppState> store, action, NextDispatcher next) {
    messageApi.start();
    next(action);
  });
}

Middleware<AppState> createStopMiddleware(MessageApi messageApi) {
  return TypedMiddleware<AppState, StopAction>(
      (Store<AppState> store, action, NextDispatcher next) {
    messageApi.stop();
    next(ClearStateAction());
  });
}

Middleware<AppState> createLoadChatsMiddleware(Repository repository) {
  final loadItems = _createLoadTodos(repository);
  return TypedMiddleware<AppState, LoadChatsAction>(loadItems);
}

Middleware<AppState> createSaveChatMiddleware(Repository repository) {
  final saveItems = _createSaveChats(repository);
  return TypedMiddleware<AppState, SaveChatsAction>(saveItems);
}

Middleware<AppState> _createSaveChats(Repository repository) {
  return (Store<AppState> store, action, NextDispatcher next) {
    next(action);
    repository.saveItems(
      store.state.chats.toList(),
    );
  };
}

Middleware<AppState> _createLoadTodos(Repository<ChatEntity> repository) {
  return (Store<AppState> store, action, NextDispatcher next) {
    repository.loadItems().then(
      (chats) {
        store.dispatch(
          ChatsLoadedAction(
            chats.toList(),
          ),
        );
      },
    ).catchError((err) => store.dispatch(ErrorAction(err)));

    next(action);
  };
}
