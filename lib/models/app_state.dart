import 'package:meta/meta.dart';
import 'package:qwil_flutter_test/domain/entity/chat_entity.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';
import 'package:qwil_flutter_test/domain/entity/summary_entity.dart';

@immutable
class AppState {
  final bool isLoading;
  final List<ChatEntity> chats;
  final List<MessageEntity> messages;
  final SummaryEntity summaryEntity;

  AppState(
      {this.isLoading = false,
      this.chats = const[],
      this.messages = const[],
      this.summaryEntity});

  factory AppState.loading() => AppState(isLoading: true);

  AppState copyWith({
    bool isLoading,
    List<ChatEntity> chats,
  }) {
    return AppState(
      isLoading: isLoading ?? this.isLoading,
      chats: chats ?? this.chats,
        messages: messages ?? this.messages
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is AppState &&
              runtimeType == other.runtimeType &&
              isLoading == other.isLoading &&
              messages == other.messages &&
              chats == other.chats;

  @override
  int get hashCode =>
      isLoading.hashCode ^
      messages.hashCode ^
      chats.hashCode;

}
