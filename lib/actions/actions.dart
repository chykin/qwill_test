import 'package:qwil_flutter_test/domain/entity/chat_entity.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';
import 'package:qwil_flutter_test/domain/entity/summary_entity.dart';

class StopAction {}
class ClearStateAction {}
class StartAction {}

class ErrorAction{
  final String message;
  ErrorAction(this.message);
}

class LoadChatsAction {}
class SaveChatsAction {}

class ChatsLoadedAction{
  final List<ChatEntity> chats;
  ChatsLoadedAction(this.chats);
  @override
  String toString() {
    return 'LoadChatsAction{chats: $chats}';
  }
}

class SummaryReadyAction{
  final SummaryEntity summaryEntity;
  SummaryReadyAction(this.summaryEntity);
  @override
  String toString() {
    return 'SummaryReadyAction{summaryEntity: $summaryEntity}';
  }
}

class NewMessageAction{
  final MessageEntity message;
  NewMessageAction(this.message);
  @override
  String toString() {
    return 'NewMessageAction{message: $message}';
  }
}