import 'package:qwil_flutter_test/actions/actions.dart';
import 'package:qwil_flutter_test/domain/entity/chat_entity.dart';
import 'package:redux/redux.dart';

final chatsReducer = combineReducers<List<ChatEntity>>([
  TypedReducer<List<ChatEntity>, ChatsLoadedAction>(_chatsLoaded),
  TypedReducer<List<ChatEntity>, NewMessageAction>(_newMessage),
  TypedReducer<List<ChatEntity>, ClearStateAction>(_clear)
]);

List<ChatEntity> _chatsLoaded(
    List<ChatEntity> chats, ChatsLoadedAction action) {
  return List.from(chats)..addAll(action.chats);
}

List<ChatEntity> _newMessage(List<ChatEntity> chats, NewMessageAction action) {
  List<ChatEntity> chatList = List.from(chats);
  chatList[0].messages..add(action.message);
  return chatList;
}

List<ChatEntity> _clear(List<ChatEntity> chats, ClearStateAction action) {
  List<ChatEntity> chatList = List.from(chats);
  chatList[0].messages..clear();
  return chatList;
}
