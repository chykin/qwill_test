 

import 'package:qwil_flutter_test/models/models.dart';
import 'package:qwil_flutter_test/reducers/chats_reducer.dart';
import 'package:qwil_flutter_test/reducers/loading_reducer.dart';
import 'package:qwil_flutter_test/reducers/summary_reducer.dart';

// We create the State reducer by combining many smaller reducers into one!
AppState appReducer(AppState state, action) {
  return AppState(
    isLoading: loadingReducer(state.isLoading, action),
    chats: chatsReducer(state.chats, action),
    summaryEntity: summaryReducer(state.summaryEntity, action)
  );
}
