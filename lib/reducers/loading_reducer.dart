 

import 'package:redux/redux.dart';
import 'package:qwil_flutter_test/actions/actions.dart';

final loadingReducer = combineReducers<bool>([
  TypedReducer<bool, ChatsLoadedAction>(_setLoaded),
  TypedReducer<bool, ErrorAction>(_setLoaded),
]);

bool _setLoaded(bool state, action) {
  return false;
}
