

import 'package:qwil_flutter_test/actions/actions.dart';
import 'package:qwil_flutter_test/domain/entity/summary_entity.dart';
import 'package:redux/redux.dart';

final summaryReducer = combineReducers<SummaryEntity>([
  TypedReducer<SummaryEntity, SummaryReadyAction>(_summaryLoaded)
]);

SummaryEntity  _summaryLoaded(SummaryEntity chats, SummaryReadyAction action) {
  return action.summaryEntity;
}
