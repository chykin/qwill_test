 

import 'package:flutter/material.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:qwil_flutter_test/routes.dart';
import 'package:qwil_flutter_test/theme.dart';
import 'package:redux/redux.dart';
import 'package:qwil_flutter_test/actions/actions.dart';
import 'package:qwil_flutter_test/middleware/store_todos_middleware.dart';
import 'package:qwil_flutter_test/models/models.dart';
import 'package:qwil_flutter_test/presentation/home_screen.dart';
import 'package:qwil_flutter_test/reducers/app_state_reducer.dart';

void main() {
  runApp(ReduxApp());
}

class ReduxApp extends StatelessWidget {
  final store = Store<AppState>(
    appReducer,
    initialState: AppState.loading(),
    middleware: createMiddleWares(),
  );

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        title: "Qwill",
        theme: ArchSampleTheme.theme,
        routes: {
          ArchSampleRoutes.home: (context) {
            return StoreBuilder<AppState>(
              onInit: (store) => store.dispatch(LoadChatsAction()),
              builder: (context, store) {
                return HomeScreen();
              },
            );
          }
        },
      ),
    );
  }
}
