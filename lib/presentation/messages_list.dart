import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:qwil_flutter_test/presentation/app_loading.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';
import 'package:qwil_flutter_test/domain/keys.dart';
import 'package:qwil_flutter_test/presentation/loading_indicator.dart';
import 'package:qwil_flutter_test/presentation/message_item.dart';

class MessagesList extends StatelessWidget {
  final List<MessageEntity> messages;
  final ScrollController _scrollController = new ScrollController();

  MessagesList({
    Key key,
    @required this.messages,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppLoading(builder: (context, loading) {
      return loading
          ? LoadingIndicator(key: ArchSampleKeys.loading)
          : _buildListView();
    });
  }

  ListView _buildListView() {

    SchedulerBinding.instance.addPostFrameCallback((_) {
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    });

    return ListView.builder(
      controller: _scrollController,
      key: ArchSampleKeys.messageList,
      itemCount: messages.length,
      itemBuilder: (BuildContext context, int index) {
        final todo = messages[index];
        return MessageItem(
          message: todo
        );
      },
    );
  }

}
