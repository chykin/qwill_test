import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';

class MessageItem extends StatelessWidget {
  final MessageEntity message;

  MessageItem({
    @required this.message,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: Text(
          message.userId,
        ),
        title: Hero(
          tag: '${message.id}__heroTag',
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Text(
              message.text,
              style: Theme.of(context).textTheme.title,
            ),
          ),
        ));
  }
}
