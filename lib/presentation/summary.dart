import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qwil_flutter_test/domain/entity/summary_entity.dart';

class SummaryWidget extends StatelessWidget {
  final SummaryEntity summaryEntity;

  SummaryWidget({Key key, this.summaryEntity}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(summaryEntity.messages.length > 0
            ? summaryEntity.messages[0].text
            : "n/a"),
        Text(summaryEntity.messages.length > 0
            ? summaryEntity.messages[1].text
            : "n/a"),
        Text(summaryEntity.messages.length > 0
            ? summaryEntity.messages[2].text
            : "n/a")
      ],
    ));
  }
}
