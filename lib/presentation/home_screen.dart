import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:qwil_flutter_test/actions/actions.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';
import 'package:qwil_flutter_test/domain/entity/summary_entity.dart';

import 'package:qwil_flutter_test/domain/keys.dart';
import 'package:qwil_flutter_test/models/app_state.dart';
import 'package:qwil_flutter_test/presentation/messages_list.dart';
import 'package:qwil_flutter_test/presentation/start_stop_controls.dart';
import 'package:qwil_flutter_test/presentation/summary.dart';
import 'package:redux/redux.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen() : super(key: ArchSampleKeys.homeScreen);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
        converter: _ViewModel.fromStore,
        builder: (context, vm) {
          return Scaffold(
            appBar: AppBar(title: Text("App")),
            body: new Container(
                child: new Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                StartStopControls(
                  key: ArchSampleKeys.startStop,
                  onStartPressed: vm.start,
                  onStopPressed: vm.stop,
                ),
                SummaryWidget(
                    key: ArchSampleKeys.summary, summaryEntity: vm.summary),
                Expanded(
                    child: MessagesList(
                        key: ArchSampleKeys.messageList, messages: vm.messages))
              ],
            )),
          );
        });
  }
}

class _ViewModel {
  final List<MessageEntity> messages;
  final Function start;
  final Function stop;
  final SummaryEntity summary;

  _ViewModel(
      {@required this.messages,
      @required this.start,
      @required this.stop,
      @required this.summary});

  static _ViewModel fromStore(Store<AppState> store) {
    return _ViewModel(
        summary: store.state.summaryEntity ?? SummaryEntity("0", []),
        messages:
            store.state.chats.length > 0 ? store.state.chats[0].messages : [],
        start: () {
          store.dispatch(StartAction());
        },
        stop: () => store.dispatch(StopAction()));
  }
}
