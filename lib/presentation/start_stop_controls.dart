import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class StartStopControls extends StatelessWidget {
  final VoidCallback onStartPressed;
  final VoidCallback onStopPressed;

  StartStopControls(
      {Key key, @required this.onStartPressed, @required this.onStopPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        RaisedButton(
          child: const Text('Start'),
          onPressed: onStartPressed,
        ),
        RaisedButton(
          child: const Text('Stop'),
          onPressed: onStopPressed,
        ),
      ],
    ));
  }
}
