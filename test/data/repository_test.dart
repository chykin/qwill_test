// Copyright 2018 The  Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'dart:async';
import 'dart:io';

import 'package:mockito/mockito.dart';
import 'package:qwil_flutter_test/data/file_storage.dart';
import 'package:qwil_flutter_test/data/messages/message_repository.dart';
import 'package:qwil_flutter_test/data/messages/messages_web_client.dart';
import 'package:qwil_flutter_test/domain/entity/message_entity.dart';
import 'package:test/test.dart';

/// We create two Mocks for our Web Client and File Storage. We will use these
/// mock classes to verify the behavior of the TodosRepository.
class MockFileStorage extends Mock implements FileStorage<MessageEntity> {}

class MockWebClient extends Mock implements MessageWebClient {}

main() {
  group('TodosRepository', () {
    List<MessageEntity> createTodos() {
      return [MessageEntity("1", "1", "1")];
    }

    test(
        'should load todos from File Storage if they exist without calling the web client',
        () {
      final fileStorage = MockFileStorage();
      final webClient = MockWebClient();
      final repository = MessageRepositoryFlutter(fileStorage, webClient);
      final todos = createTodos();

      // We'll use our mock throughout the tests to set certain conditions. In
      // this first test, we want to mock out our file storage to return a
      // list of Todos that we define here in our test!
      when(fileStorage.loadItems()).thenAnswer((_) => Future.value(todos));

      expect(repository.loadItems(), completion(todos));
      verifyNever(webClient.fetch());
    });

    test(
        'should fetch todos from the Web Client if the file storage throws a synchronous error',
        () async {
      final fileStorage = MockFileStorage();
      final webClient = MockWebClient();
      final repository = MessageRepositoryFlutter(fileStorage, webClient);
      final todos = createTodos();

      // In this instance, we'll ask our Mock to throw an error. When it does,
      // we expect the web client to be called instead.
      when(fileStorage.loadItems()).thenThrow("Uh ohhhh");
      when(webClient.fetch()).thenAnswer((_) => Future.value(todos));

      // We check that the correct todos were returned, and that the
      // webClient.fetch method was in fact called!
      expect(await repository.loadItems(), todos);
      verify(webClient.fetch());
    });

    test(
        'should fetch todos from the Web Client if the File storage returns an async error',
        () async {
      final fileStorage = MockFileStorage();
      final webClient = MockWebClient();
      final repository = MessageRepositoryFlutter(fileStorage, webClient);
      final todos = createTodos();

      when(fileStorage.loadItems()).thenThrow(Exception("Oh no."));
      when(webClient.fetch()).thenAnswer((_) => Future.value(todos));

      expect(await repository.loadItems(), todos);
      verify(webClient.fetch());
    });

    test('should persist the todos to local disk and the web client', () {
      final fileStorage = MockFileStorage();
      final webClient = MockWebClient();
      final repository = MessageRepositoryFlutter(fileStorage, webClient);
      final todos = createTodos();

      when(fileStorage.saveItems(todos))
          .thenAnswer((_) => Future.value(File('falsch')));
      when(webClient.postTodos(todos)).thenAnswer((_) => Future.value(true));

      // In this case, we just want to verify we're correctly persisting to all
      // the storage mechanisms we care about.
      expect(repository.saveItems(todos), completes);
      verify(fileStorage.saveItems(todos));
      verify(webClient.postTodos(todos));
    });
  });
}
